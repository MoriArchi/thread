import * as authService from 'src/services/authService';
import {
  SET_USER,
  SET_EXPANDED_EDIT_IMAGE_PROFILE,
  SET_EXPANDED_EDIT_USERNAME_PROFILE,
  EDIT_PROFILE
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

const setExpandedEditImageProfileAction = user => ({
  type: SET_EXPANDED_EDIT_IMAGE_PROFILE,
  user
});

const setExpandedEditUsernameProfileAction = user => ({
  type: SET_EXPANDED_EDIT_USERNAME_PROFILE,
  user
});

const editProfileAction = user => ({
  type: EDIT_PROFILE,
  user
});

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const toggleExpandedEditImageProfile = user => async dispatch => {
  const current = user ? await authService.getCurrentUser() : undefined;
  dispatch(setExpandedEditImageProfileAction(current));
};

export const toggleExpandedEditUsernameProfile = user => async dispatch => {
  const current = user ? await authService.getCurrentUser() : undefined;
  dispatch(setExpandedEditUsernameProfileAction(current));
};

export const editProfile = () => async dispatch => {
  const updatedProfile = await authService.getCurrentUser();
  dispatch(editProfileAction(updatedProfile));
};
